// console.log("Hello you are now linked!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
let printUserInfo = function(){
	let userName = prompt("Enter your Name: ");
	let userAge = prompt("How old are you? ");
	let userCityAddress = prompt("What city do you live in?");

	console.log("Hello, "+userName);
	console.log("You are "+userAge+" years old.");
	console.log("You live in "+userCityAddress);
}

printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

// USING PROMPT:

let userFaveBands = function(){
	let userFaveBand1 = prompt("Enter one of your top 5 favorite band of all time: (*1)");
	let userFaveBand2 = prompt("Enter one of your top 5 favorite band of all time: (*2)");
	let userFaveBand3 = prompt("Enter one of your top 5 favorite band of all time: (*3)");
	let userFaveBand4 = prompt("Enter one of your top 5 favorite band of all time: (*4)");
	let userFaveBand5 = prompt("Enter one of your top 5 favorite band of all time: (*5)");

	console.log(
		"1. "+userFaveBand1,
		"\n2. "+userFaveBand2,
		"\n3. "+userFaveBand3,
		"\n4. "+userFaveBand4,
		"\n5. "+userFaveBand5)
}

// userFaveBands();

// For console only:

let fiveFaveBand = function(){
	faveBand1 = "Eraserheads";
	faveBand2 = "Parokya ni Edgar";
	faveBand3 = "Kamikazee";
	faveBand4 = "Spongecola";
	faveBand5 = "Imago";

	console.log("1. "+faveBand1);
	console.log("2. "+faveBand2);
	console.log("3. "+faveBand3);
	console.log("4. "+faveBand4);
	console.log("5. "+faveBand5);
}

fiveFaveBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

// For console only: 

let fiveFaveMovie = function(){
	faveMovie1 ="1. The Godfather \nRotten Tomatoes Rating: 97%";
	faveMovie2 = "1. The Godfather, Part II \nRotten Tomatoes Rating: 96%";
	faveMovie3 = "Everything Everywhere All At Once\nRotten Tomatoes Rating: 94%";
	faveMovie4 = "Avengers: Endgame\nRotten Tomatoes Rating: 94%";
	faveMovie5 = "Avengers: Infinity War\nRotten Tomatoes Rating: 85%";

	console.log(faveMovie1);
	console.log(faveMovie2);
	console.log(faveMovie3);
	console.log(faveMovie4);
	console.log(faveMovie5);
}

fiveFaveMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printFriends();
